package com.kodelabs.boilerplate.domain.interactors;

import com.kodelabs.boilerplate.domain.interactors.base.Interactor;

/**
 * Created by yaroslavshlapak on 08.11.16.
 */

public interface WelcomingInteractor extends Interactor {

    interface Callback {
        void onMessageRetreived(String message);
        void onRetrievalFailed(String error);
    }
}
