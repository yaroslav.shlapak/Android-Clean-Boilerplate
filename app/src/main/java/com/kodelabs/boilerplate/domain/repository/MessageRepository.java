package com.kodelabs.boilerplate.domain.repository;

/**
 * Created by yaroslavshlapak on 08.11.16.
 */

public interface MessageRepository {
    String getWelcomeMessage();
}
