package com.kodelabs.boilerplate.domain.interactors.impl;

import com.kodelabs.boilerplate.domain.executor.Executor;
import com.kodelabs.boilerplate.domain.executor.MainThread;
import com.kodelabs.boilerplate.domain.interactors.WelcomingInteractor;
import com.kodelabs.boilerplate.domain.interactors.base.AbstractInteractor;
import com.kodelabs.boilerplate.domain.repository.MessageRepository;

/**
 * Created by yaroslavshlapak on 08.11.16.
 */

public class WelcomingInteractorImpl extends AbstractInteractor implements WelcomingInteractor {
    private WelcomingInteractor.Callback mCallback;
    private MessageRepository mMessageRepository;

    public WelcomingInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                   WelcomingInteractor.Callback callback , MessageRepository repository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mMessageRepository = repository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {

            @Override
            public void run() {
                mCallback.onRetrievalFailed("Nothing to welcome you with :(");
            }
        });
    }

    private void postMessage(final String msg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMessageRetreived(msg);
            }
        });
    }

    @Override
    public void run() {
        final String message = mMessageRepository.getWelcomeMessage();

        if(message == null || message.length() == 0) {
            notifyError();

            return;
        }

        postMessage(message);
    }
}
